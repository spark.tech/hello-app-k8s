# Hello App
A sample application to connect to DB hosted on kubernetes.

#### Tech stack used
- python flask
- mongodb
- Kubernetes (minikube)
    -   Statefulsets
    -   secrets
    -   helm charts for deployment

#### Steps for deployment on minikube

##### Step1: Minikube setup

Execute the below command after installing minikube and virtual box. Used virtual box so that ingress can be enabled
```sh
$ minikube start --driver=virtualbox --vm=true
```
When minikube has started sucessfully enable ingress by executing the below command
```sh
$ minikube addons enable ingress 
```

Also check if the storage is enabled in the minikube by using the minikube command
```sh
$ minikube addons list
```

##### Step2: Build the required images

Before building images to utilize the images in the local machine we need to execute the below command
```sh
$ eval $(minikube -p minikube docker-env)   
```
Now after cloning the repository execute the commands from the root folder of the git repository, i.e after cloning cd into hello-app-k8s folder to execute all the commands henceforth

- Build the required images one by one
```sh
$ docker build ./app -t hello-app
$ docker build ./db/seed-data -t seedjob-db
```
 The app folder consists of the code for the application and the dockerfile for the app
 The db folder consists the dockerfile to run the seed job for the mongodb to populate data
 
 ##### Step3: Kubernetes setup
 
 - We create the namespace required from the manifest file inside k8s folder. Execute the below command to create the namespace '**_app_**'
 ```
 $ kubectl apply -f ./k8s -n app
 ```
 - Next we need to create the secret for connecting to the database. Execute the below command without changing the username and the password
 ```
 $ kubectl create secret generic database --from-literal=username=hello --from-literal=password=check -n app
 ```
 
 ##### Step5: Deploy the app via helm
 
 Execute the below commands one by one to deploy the db first and then the python app
 ```
$ helm install mongo-db ./helm/db --namespace app
$ helm install hello-ap ./helm/hello-app/ --values ./helm/hello-app/values.yaml --namespace app
 ```
 _note_: the seed job also gets executed via the db installation chart. The seed job will error (error status) out initially as mongodb takes some time to initiate, however the pod retries for a maximum of 5 times to change the status to _complete_ like below

 ![command line](./cmd-line-hello.png)
 
 Below is the screen shot of the app, it just displays the value from database without any filters

 ![hello-app on kubernetes](./hello-app.png)
 