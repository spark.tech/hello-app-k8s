import os
from flask import Flask, jsonify, request
from bson.json_util import dumps, loads
from bson.objectid import ObjectId
from pymongo import MongoClient


app = Flask("__name__")

db_username = os.environ.get("DATABASE_USERNAME")
db_password = os.environ.get("DATABASE_PASSWORD")
db_url="mongodb://"+db_username+":"+db_password+"@mongo-db:27017/hello"
db_client = MongoClient(db_url)

db = db_client['hello']

@app.route('/count')
def root_path():    
    _all_data = db.page.find().count()
    return str(_all_data)
    

@app.route('/')
def get_people():
    _all_data = db.page.find()
    _response = dumps(_all_data)
    return _response

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=False)
